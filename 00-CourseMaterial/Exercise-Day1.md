
## Exercise 1: Language Constructs

In this exercise you get to know certain JavaScript language constructs by completing "automated lessons":

- Change into the directory `02-Language/90-Lessons`
- Start a web server in the directory and open `index.html` in the browser:

```
npm install
npm start
```

- You will see some failing tests.
- Open the corresponding JavaScript file in an editor. i.e: `00-CourseSelection.js`. Read the code of corresponding lesson. 
- Edit the code, so that the first Tests should be passing.
- Refresh the browser. The test should turn green.
- Advance to the next test.



## Exercise 2: Scope

Inspect the code in the example `02-Language/91-CapturingScope/index.html`. Click on the numbers and the Button: Did you expect the given behaviour?  

Change the code, so that:

- when you click on a number then that number is displayed in the alert dialog 
- clicking the button shows an increasing number in the alert

Try to solve the problem with ES2015 constructs (`let` and `for-of`-loop).
Advanced: Try to solve the problem wich `var` only, by introducing closures.



## Exercise 3: From Spaghetti to Modules

Inspect the example `02-Language/92-Spaghetti`: Include the new functionality on the page by un-commenting the commented code in `index.html`.  
Why is there a side-effect form the newly included functionality on the existing functionality?

Change the example, so that there are no side-effects between the existing and the newly included functionality.

Try to solve the problem with ES2015 modules. 
Advanced: Solve the problem without ES2015 modules. Use IIFEs/closures to wrap each functionality in a module that is completely isolated.


