namespace Organization {
    export class Person {
        constructor(private firstName: string, private lastName: string) { }

        getFullName() {
            return `${this.firstName} ${this.lastName}`
        }
    }

    export class Employee extends Person {
        constructor(firstName: string, lastName: string, private company: string) {
            super(firstName, lastName);
        }
        
        getDescription(){
            return `${this.getFullName()}, ${this.company}`;
        }
    }
}

let Employee = Organization.Employee;
let employee = new Employee('Tyler', 'Durden', 'Fight Club');
console.log(employee.getDescription());