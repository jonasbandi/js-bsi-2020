lesson('About This Exercise', function () {

    learn('how checks are working ', function () {
        var valueToCheck = 42;
        expect(valueToCheck).toBe(FILL_ME_IN);
    });
});


lesson('About Variable Scope', function () {

    learn('the difference between local and global variables', function () {
        var isLocal = FILL_ME_IN;
        isGlobal = FILL_ME_IN; //eslint-disable-line

        expect(isLocal).toBe(42);
        expect(window.isGlobal).toBe(42); //global variables are assigned to the window object
    });

    learn('that there is no block scope with ``var', function () {

        var x = 'Hello!';

        if(true){
            var x = 'Bye!'; // x inside the block is the same instance as x outside the block
        }

        expect(x).toBe(FILL_ME_IN);
    });

});


lesson('About Functions', function () {

    learn('that functions are objects', function () {

        function greet(){
            return 'Hello!';
        }

        // You can put functions into an array
        var functionsCollection = [parseInt, setTimeout];
        functionsCollection.push(greet);

        expect(functionsCollection.length).toBe(FILL_ME_IN);

        // you can assign functions to variables or objects
        var obj = {};
        obj.sayHello = greet;
        var greeterFunc = obj.sayHello;

        expect(obj.sayHello()).toBe(FILL_ME_IN);
        expect(greeterFunc()).toBe(FILL_ME_IN);

        // functions can have properties
        greet.owner = 'The Architect';

        expect(greet.owner).toBe(FILL_ME_IN);
    });

    learn('that functions have some default properties', function () {
        function greet(){
            return 'Hello!';
        }

        expect(greet.name).toBe(FILL_ME_IN);
        expect(greet.call !== undefined).toBe(FILL_ME_IN);
        expect(typeof greet.call).toBe(FILL_ME_IN);
        expect(greet.apply !== undefined).toBe(FILL_ME_IN);
        expect(typeof greet.apply).toBe(FILL_ME_IN);
        expect(greet.bind !== undefined).toBe(FILL_ME_IN);
        expect(typeof greet.bind).toBe(FILL_ME_IN);
        expect(greet.prototype !== undefined).toBe(FILL_ME_IN);

    });

    learn('that a function can be used as a constructor', function () {
        function Person(name) {
            this.name = name;
        }

        var pers = new Person('Tyler');
        expect(pers.name).toBe(FILL_ME_IN);
    });

});


lesson('About Objects', function () {

    learn('that objects can inherit from other objects', function () {

        var animal = { eat: function () { return 'Mampf!'; } };

        var bird = Object.create(animal);
        bird.sing = function () { return 'Beep!';  };

        var sparrow = Object.create(bird);
        var nightingale = Object.create(bird);
        nightingale.sing = function () { return 'lalalala'};

        expect(sparrow.eat()).toBe(FILL_ME_IN);
        expect(sparrow.sing()).toBe(FILL_ME_IN);
        expect(nightingale.eat()).toBe(FILL_ME_IN);
        expect(nightingale.sing()).toBe(FILL_ME_IN);

        animal.eat = function () { return 'Schmatz!'; };
        expect(sparrow.eat()).toBe(FILL_ME_IN);
        expect(nightingale.eat()).toBe(FILL_ME_IN);

        expect(nightingale.eat === animal.eat).toBe(FILL_ME_IN);
        expect(nightingale.hasOwnProperty('eat')).toBe(FILL_ME_IN);
    });


    learn('how to use the constructor prototype to add properties to all objects', function () {

        // this is a constructor function
        function Circle(radius) {
            this.radius = radius;
        }

        // creating instances by calling the constructor function
        var simpleCircle = new Circle(10);
        var colouredCircle = new Circle(5);
        colouredCircle.colour = 'red';

        expect(simpleCircle.colour).toBe(FILL_ME_IN);
        expect(colouredCircle.colour).toBe(FILL_ME_IN);

        Circle.prototype.describe = function () {
            return 'This circle has a radius of: ' + this.radius;
        };

        expect(simpleCircle.describe()).toBe(FILL_ME_IN);
        expect(colouredCircle.describe()).toBe(FILL_ME_IN);
        expect(simpleCircle.describe === Circle.prototype.describe).toBe(FILL_ME_IN);
    });

    learn('the difference between instance properties and prototype properties', function () {

        function Person(name){
            this.name = name;
            this.greet = function(){ return 'Greetings!'};
        }
        Person.prototype.sayHello = function(){ return 'Hello!'};

        var p1 = new Person('Tyler');
        var p2 = new Person('Durden');

        expect(p1.greet()).toBe(FILL_ME_IN);
        expect(p2.greet()).toBe(FILL_ME_IN);
        expect(p1.sayHello()).toBe(FILL_ME_IN);
        expect(p2.sayHello()).toBe(FILL_ME_IN);
        expect(p1.sayHello === p2.sayHello).toBe(FILL_ME_IN);
        expect(p1.greet === p2.greet).toBe(FILL_ME_IN);
    });
});


lesson('About The Value Of `This`', function () {

    learn('this is bound when calling the function', function () {

        function sayHello(){ return 'Hello from ' + this.origin}

        var obj = {
            greet: sayHello,
            origin: 'Object'
        };

        function execute(func){
            return func();
        }

        window.origin = 'Global Scope';

        expect(obj.greet()).toBe(FILL_ME_IN);
        expect(sayHello()).toBe(FILL_ME_IN);
        expect(execute(obj.greet)).toBe(FILL_ME_IN);
        expect(sayHello.call({origin: 'Call Context'})).toBe(FILL_ME_IN);
        expect(sayHello.apply({origin: 'Call Context'})).toBe(FILL_ME_IN);
        expect(obj.greet.call({origin: 'Call Context'})).toBe(FILL_ME_IN);

    });
});


lesson('About Closures', function () {

    learn('that a closure can capture a value', function () {

        var message = 'Hello';

        function createGreetingFunction(message){
            return function(){
                return message; // message is captured in the closure
            }
        }

        myGreeter = createGreetingFunction(message); // the inner function still has access to its context via its closure

        message = 'Bye!';

        expect(myGreeter()).toBe(FILL_ME_IN);
    });


    learn('that there is no block scope', function () {
        var arr = [1, 2, 3];
        var out = [];

        for (var i = 0; i < arr.length; i++) {
            var item = arr[i]; // there is only one 'item' variable that lives on the scope of the outer function
            out.push(function () {
                return item;
            });
        }

        var result = '';
        out.forEach(function (func) {
            result += func();
        });

        expect(result).toBe(FILL_ME_IN);
    });

    learn('how to use closures to capture values during iteration', function () {
        var arr = [1, 2, 3];
        var out = [];

        for (var i = 0; i < arr.length; i++) {
            (function () {
                var item = arr[i]; // this is a variable with "private" function scope
                out.push(function () {
                    return item;
                });
            })(); // this is called an "Immediately Invoked Function Expression" (IIFE)
        }

        var result = '';
        out.forEach(function (func) {
            result += func();
        });

        expect(result).toBe(FILL_ME_IN);
    });
});

lesson('About Classes', function () {
    learn('how `this` still is dynamically bound in ES6 classes', () => {
        class Person {
            constructor(name) {
                this.name = name;
            }

            greet() {
                return `Hello, my name is ${this.name}`;
            }
        }

        let p = new Person('Bob');

        expect(p.greet.call({name: 'Alice'})).toBe(FILL_ME_IN);
        // This illustrates the problem from more real-world scenarios like passing greet as an event-handler or callback

    });

    learn('how `this` can be bound to an instance', () => {
        class Person {
            constructor(name) {
                this.name = name;
                this.greet = this.greet.bind(this);
            }

            greet() {
                return `Hello, my name is ${this.name}`;
            }
        }

        let p = new Person('Bob');

        expect(p.greet.call({name: 'Alice'})).toBe(FILL_ME_IN);
    });
});

lesson('about strict mode', function () {
    learn('about strict mode', function () {

        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions_and_function_scope/Strict_mode
        // http://www.w3schools.com/js/js_strict.asp
        // http://msdn.microsoft.com/en-us/library/br230269(v=vs.94).aspx

        function whatIsThisWithoutMode() {
            // Apply the '!' operator to an object to get 'true' or 'false' back
            return !!this; // 'this' refers to global object
        }

        function whatIsThisInStrictMode() {
            'use strict';
            // Apply the '!' operator to an object to get 'true' or 'false' back
            return !!this; // in strict mode, the keyword 'this' does not refer to the global object, unlike traditional JS. So here,'this' is undefined.
        }

        expect(whatIsThisWithoutMode()).toBe(FILL_ME_IN);
        expect(whatIsThisInStrictMode()).toBe(FILL_ME_IN);

    });
});

