console.log('loading first module');

// Static import
// import {sayHello} from './second.js';
// sayHello();


//Dynamic import
document.getElementById('loader')
  .addEventListener('click', async () => {

    const {sayHello} = await import(/* webpackChunkName: 'module2' */ './second.js');
    sayHello();

  });


