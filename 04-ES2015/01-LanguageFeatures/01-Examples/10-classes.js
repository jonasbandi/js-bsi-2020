class Polygon {
    constructor(height, width) { //class constructor
        this.name = 'Polygon';
        this.height = height;
        this.width = width;
    }

    sayName() { //class method
        console.log('Hi, I am a', this.name + '.');
    }

    static convert(value){
        return value * 1.3;
    }
}

class Square extends Polygon {
    constructor(length) {
        super(length, length); //call the parent method with super
        this.name = 'Square';
    }

    get area() { //calculated attribute getter
        return this.height * this.width;
    }
}

let s = new Square(5);

s.sayName();
console.log(s.area);

console.log("Converted: " + Polygon.convert(2));

//setTimeout(s.sayName, 1); // issues with scoping this remain!

// Classes are syntactic sugar over existing prototype based inheritance:
//console.log(s.__proto__);
//console.log(s.__proto__.__proto__);
