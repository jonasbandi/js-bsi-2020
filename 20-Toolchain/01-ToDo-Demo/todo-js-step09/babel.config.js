/*eslint-env node */
module.exports = function(api) {
  const presets = [['@babel/env', {
    targets: {
      ie: '11',
      edge: '17',
      firefox: '60',
      chrome: '67',
      safari: '11.1'
    },
    useBuiltIns: 'usage',
    modules: false
  }]];
  const plugins = [];

  api.cache(true);

  return {
    presets,
    plugins
  };
};