import model from './model';

test('model is created', () => {
  expect(model).toBeDefined();
});

test('add item', () => {
  const itemCount = model.toDoList.length;
  model.addToDo();
  expect(model.toDoList).toHaveLength(itemCount + 1);
});

test('remove item', () => {
  const itemCount = model.toDoList.length;
  model.removeToDo(model.toDoList[0]);
  expect(model.toDoList).toHaveLength(itemCount -1 );
});
