/* eslint-env node */

module.exports = {
    testPathDirs: ['src/js'],
    reporters: ['default', ['jest-junit', { outputDirectory: './build-info' }]]
};
