# Public URL:

https://spa-demos.herokuapp.com/


# Starting the app locally:

    npm i --prefix 01-ToDo-Server
    npm i --prefix 02-ToDo-SPA
    npm i --prefix 03-ToDo-SPA-API
    npm i
    npm run build
    npm start

Then go to: http://localhost:4001/
  

# Deploying to Heroku

    docker build --tag spa-demos .
    
    heroku login
    
    heroku container:login
    
    heroku container:push web --app spa-demos
    
    heroku container:release web --app spa-demos


