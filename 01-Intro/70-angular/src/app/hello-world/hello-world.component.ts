import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  template: `
    <h1>{{message}}</h1>
    <button (click)="reverse()">Reverse!</button>
  `
})
export class HelloWorldComponent {
  message = 'HelloWorld';

  reverse() {
    this.message = this.message
      .split('')
      .reverse()
      .join('');
  }
}
